import django
from django.contrib import admin
from api import views
from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework.authtoken import views as auth
from rest_framework.routers import SimpleRouter
from api import views


router = SimpleRouter()

router.register(r'categories', views.CategoriesViewSet, 'Categories')
router.register(r'user/categories', views.UserCategoriesViewSet, 'Categories')
router.register(r'itemcategory', views.ItemCategoryViewSet, 'ItemCategory')
router.register(r'user/itemcategory', views.UserItemCategoryViewSet, 'ItemCategory')
router.register(r'itempromo', views.ItemPromoViewSet, 'ItemPromo')
router.register(r'user/itempromo', views.UserItemPromoViewSet, 'ItemPromo')
router.register(r'promos', views.PromosViewSet, 'Promos')
router.register(r'user/promos', views.UserPromosViewSet, 'Promos')
router.register(r'items', views.ItemsViewSet, 'Items')
router.register(r'user/items', views.UserItemsViewSet, 'Items')
router.register(r'orderdetail', views.OrderDetailViewSet, 'OrderDetail')
router.register(r'user/orderdetail', views.UserOrderDetailViewSet, 'OrderDetail')
router.register(r'orders', views.OrdersViewSet, 'Orders')
router.register(r'user/orders', views.UserOrdersViewSet, 'Orders')
router.register(r'related', views.RelatedViewSet, 'Related')
router.register(r'user/related', views.UserRelatedViewSet, 'Related')
router.register(r'users', views.UserViewSet, 'User')
urlpatterns = router.urls
urlpatterns = format_suffix_patterns(urlpatterns)
