from rest_framework.serializers import ModelSerializer
from api.models import Categories, ItemCategory, ItemPromo, Promos, Items, OrderDetail, Orders, Related
from .orderDetail import OrderDetailSerializer
from rest_framework import serializers
from datetime import datetime
from django.db.models import Count, Min, Sum, Avg
from django.db.models.functions import Coalesce
from django.contrib.auth.models import User
class OrdersSerializer(ModelSerializer):
    details = OrderDetailSerializer(many=True, write_only=True)
    customer = serializers.PrimaryKeyRelatedField(read_only=True)
    customer_id = serializers.PrimaryKeyRelatedField(write_only=True, queryset=User.objects.all())
    class Meta:
        model = Orders
        fields = "__all__"
    def validate_status(self, value):
        if self.instance and ((value not in [-1, 0, 1, 2]) or (value is not -1 and value < self.instance.status) or (value > self.instance.status+1) or (self.instance.status is -1 and value is not -1) or (self.instance.status is not 0 and value is -1)):
            raise serializers.ValidationError("Action is not allowed")
        return value   
    def create(self, validated_data):
        details = validated_data.pop('details')
        customer = validated_data.pop('customer_id')
        order = Orders.objects.create(customer_id=customer.pk, **validated_data)
        for detail in details:
            detail['orders_id'] = order
            od = OrderDetailSerializer.create(self, validated_data=detail)
        return order
    def update(self, instance, validated_data):
        status = validated_data.get('status')
        current_status = self.instance.status
        instance = super(OrdersSerializer, self).update(instance, validated_data)
        # Update quantity if the order is cancelled (only when the order is still activated)
        if status is -1 and current_status is not -1:
            details = OrderDetail.objects.filter(orders=instance.id)
            for detail in details:
                # raise serializers.ValidationError(str(detail))
                item = Items.objects.get(id=detail.item.id)
                item.quantity += detail.quantity
                item.save()
        return instance    
    