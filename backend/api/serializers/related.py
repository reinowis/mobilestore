from rest_framework.serializers import ModelSerializer
from api.models import Categories, ItemCategory, ItemPromo, Promos, Items, OrderDetail, Orders, Related

class RelatedSerializer(ModelSerializer):

    class Meta:
        model = Related
        fields = '__all__'
    