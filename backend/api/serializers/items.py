from rest_framework import serializers
from rest_framework.serializers import ModelSerializer
from api.models import Categories, ItemCategory, ItemPromo, Promos, Items, OrderDetail, Orders, Related 
from .categories import CategoriesSerializer
from .promos import PromosSerializer
from rest_framework import response
from django.db.models import Count, Min, Sum, Avg
from django.db.models.functions import Coalesce
class ItemsSerializer(ModelSerializer):
    categories = serializers.PrimaryKeyRelatedField(many=True,read_only=True)
    categories_id = serializers.PrimaryKeyRelatedField(many=True,write_only=True, queryset=Categories.objects.all(), required=False)
    promos = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    promos_id = serializers.PrimaryKeyRelatedField(many=True, write_only=True, queryset=Promos.objects.all(), required=False)
    related = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    related_id = serializers.PrimaryKeyRelatedField(many=True, write_only=True, queryset=Items.objects.all(), required=False)
    promo_price = serializers.SerializerMethodField()
    class Meta:
        model = Items
        fields = '__all__'
    def create(self, validated_data):
        categories = []
        promos = []
        related = []
        if (validated_data.get('categories_id')):
            categories = validated_data.pop('categories_id')
        if (validated_data.get('promos_id')):
            promos = validated_data.pop('promos_id')
        if (validated_data.get('related_id')):
            related = validated_data.pop('related_id')
        item = Items.objects.create(**validated_data)
        for category in categories:
            ItemCategory.objects.create(item=item, category=category)
        for promo in promos:
            try:
                ItemPromo.objects.create(item=item, promo=promo)
            except Promos.DoesNotExist:
                raise serializers.ValidationError("Promo not found")
        for r in related:
            Related.objects.create(item=item, related=r)
        return item
    def update(self, instance, validated_data):
        categories = []
        promos = []
        related = []
        if (validated_data.get('categories_id')):
            categories = validated_data.pop('categories_id')
        if (validated_data.get('promos_id')):
            promos = validated_data.pop('promos_id')
        if (validated_data.get('related_id')):
            related = validated_data.pop('related_id')
        current_categories = self.instance.categories.all()
        current_promos = self.instance.promos.all()
        current_related = self.instance.related.all()
        instance = super(ItemsSerializer, self).update(instance, validated_data)
        #  update categories list
        cate_delete = [x for x in current_categories if x not in categories]
        cate_create = [x for x in categories if x not in current_categories]
        for item in cate_delete:
            ItemCategory.objects.filter(item=instance, category = item).delete()
        for item in cate_create:
            ItemCategory.objects.create(item=instance, category = item)
        #  update promos list
        promo_delete = [x for x in current_promos if x not in promos]
        promo_create = [x for x in promos if x not in current_promos]
        for item in promo_delete:
            ItemPromo.objects.filter(item=instance, promo = item).delete()
        for item in promo_create:
            ItemPromo.objects.create(item=instance, promo = item)    
        #  update related list
        rel_delete = [x for x in current_related if x not in related]
        rel_create = [x for x in related if x not in current_related]
        for item in rel_delete:
            Related.objects.filter(item=instance, related = item).delete()
        for item in rel_create:
            Related.objects.create(item=instance, related = item)        
        return instance
    def get_promo_price(self, obj):
        promos = obj.promos.all().values('items').aggregate(total=Coalesce(Sum('value'),0))
        return int((100-promos['total']) * obj.price /100)