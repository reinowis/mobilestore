from rest_framework.serializers import ModelSerializer
from api.models import Categories, ItemCategory, ItemPromo, Promos, Items, OrderDetail, Orders, Related
from django.contrib.auth.models import User
class UserSerializer(ModelSerializer):

    class Meta:
        model = User
        fields = '__all__'
    def create(self, validated_data):
        user = User.objects.create_user(**validated_data)
        return user        