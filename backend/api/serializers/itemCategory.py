from rest_framework.serializers import ModelSerializer
from api.models import Categories, ItemCategory, ItemPromo, Promos, Items, OrderDetail, Orders, Related
class ItemCategorySerializer(ModelSerializer):

    class Meta:
        model = ItemCategory
        fields = '__all__'
        auto_created = True

