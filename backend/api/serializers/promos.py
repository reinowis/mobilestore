from rest_framework.serializers import ModelSerializer
from api.models import Categories, ItemCategory, ItemPromo, Promos, Items, OrderDetail, Orders, Related
from rest_framework import serializers
from .itemPromo import ItemPromoSerializer
class PromosSerializer(ModelSerializer):
    promo_items = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    promo_items_id = serializers.PrimaryKeyRelatedField(many=True,write_only=True, queryset=Items.objects.all(), required=False)
    class Meta:
        model = Promos
        fields = '__all__'
    def create(self, validated_data):
        items = []
        if (validated_data.get('promo_items_id')):
            items = validated_data.pop('promo_items_id')
        promo = Promos.objects.create(**validated_data)
        for item in items:
            ItemPromo.objects.create(item=item, promo=promo)
        return promo
    def update(self, instance, validated_data):
        items = []
        if (validated_data.get('promo_items_id')):
            items = validated_data.pop('promo_items_id')
        current_item = self.instance.promo_items.all()
        instance = super(PromosSerializer, self).update(instance, validated_data)
        delete = [x for x in current_item if x not in items]
        create = [x for x in items if x not in current_item]
        for item in delete:
            ItemPromo.objects.filter(promo=instance, item = item).delete()
        for item in create:
            ItemPromo.objects.create(promo=instance, item = item)
        return instance    