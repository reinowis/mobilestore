from rest_framework.serializers import ModelSerializer
from api.models import Categories, ItemCategory, ItemPromo, Promos, Items, OrderDetail, Orders, Related
class ItemPromoSerializer(ModelSerializer):

    class Meta:
        model = ItemPromo
        fields = '__all__'
