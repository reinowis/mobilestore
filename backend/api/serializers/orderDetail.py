from rest_framework.serializers import ModelSerializer
from api.models import Categories, ItemCategory, ItemPromo, Promos, Items, OrderDetail, Orders, Related
from rest_framework import serializers
from .items import ItemsSerializer
from django.utils import formats
from datetime import datetime
from django.db.models import Count, Min, Sum, Avg
from django.db.models.functions import Coalesce
class OrderDetailSerializer(ModelSerializer):
    orders = serializers.PrimaryKeyRelatedField(read_only=True)
    orders_id = serializers.PrimaryKeyRelatedField(write_only=True, queryset = Orders.objects.all(),allow_null=True)
    item = serializers.PrimaryKeyRelatedField(read_only=True)
    item_id = serializers.PrimaryKeyRelatedField(write_only=True, queryset=Items.objects.all())
    class Meta:
        model = OrderDetail
        fields = '__all__'
    def create(self, validated_data):
        orders = validated_data.get('orders_id')
        date = datetime.now().date()
        time = datetime.now().time()
        weekdays = datetime.now().weekday()
        item = validated_data.get('item_id')
        promos = item.promos.filter(date_start__lte=date,date_end__gte=date,dates__icontains=weekdays, time_start__lte=time, time_end__gte=time).values('items').aggregate(total=Coalesce(Sum('value'),0))
        quantity = validated_data.get('quantity')
        price = validated_data['price'] = (100-promos['total']) * item.price / 100
        total = validated_data['total'] = price*quantity
        # Update items - quantity
        if (validated_data['quantity'] > item.quantity):
            raise serializers.ValidationError('Quantity is not allowed')
        try:
            orderDetail = OrderDetail.objects.get(orders=orders.id, item=item.id)
            orderDetail.quantity += quantity
            orderDetail.total = price*orderDetail.quantity
            orderDetail.save()
        except OrderDetail.DoesNotExist:    
            orderDetail = OrderDetail.objects.create(orders_id=orders.id, item_id=item.id, price=price, quantity=quantity, total=total)
        # Update order
        od = OrderDetail.objects.filter(orders=orders).values('orders').aggregate(total=Coalesce(Sum('total'),0))
        orders.price = od['total']
        orders.save()
        # Update quantity
        item.quantity = item.quantity - validated_data['quantity']
        item.save()
        return orderDetail
        