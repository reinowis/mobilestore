from rest_framework.serializers import ModelSerializer
from api.models import Categories, ItemCategory, ItemCategory, Categories, Items, OrderDetail, Orders, Related
from rest_framework import serializers
class CategoriesSerializer(ModelSerializer):
    cate_items = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    cate_items_id = serializers.PrimaryKeyRelatedField(many=True,write_only=True, queryset=Items.objects.all(), required=False)
    class Meta:
        model = Categories
        fields = '__all__'
    def create(self, validated_data):
        items = []
        if (validated_data.get('cate_items_id')):
            items = validated_data.pop('cate_items_id')
        catgegory = Categories.objects.create(**validated_data)
        for item in items:
            ItemCategory.objects.create(item=item, category=catgegory)
        return catgegory
    def update(self, instance, validated_data):
        items = []
        if (validated_data.get('cate_items_id')):
            items = validated_data.pop('cate_items_id')
        current_item = self.instance.cate_items.all()
        instance = super(CategoriesSerializer, self).update(instance, validated_data)
        delete = [x for x in current_item if x not in items]
        create = [x for x in items if x not in current_item]
        for item in delete:
            ItemCategory.objects.filter(category=instance, item = item).delete()
        for item in create:
            ItemCategory.objects.create(category=instance, item = item)
        return instance    
