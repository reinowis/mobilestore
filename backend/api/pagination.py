from rest_framework import pagination
from rest_framework.response import Response
class ContentRangeHeaderPagination(pagination.PageNumberPagination):
    """
    A custom Pagination class to include Content-Range header in the
    response.
    """

    def get_paginated_response(self, data):
        """
        Override this method to include Content-Range header in the response.

        For eg.:
        Sample Content-Range header value received in the response for 
        items 11-20 out of total 50:

                Content-Range: items 10-19/50
        """

        total_items = self.page.paginator.count # total no of items in queryset
        item_starting_index = self.page.start_index() - 1 # In a page, indexing starts from 1
        item_ending_index = self.page.end_index() - 1

        content_range = 'items {0}-{1}/{2}'.format(item_starting_index, item_ending_index, total_items)      

        headers = {'Content-Range': content_range} 

        return Response(data, headers=headers)