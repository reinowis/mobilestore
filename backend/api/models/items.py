from __future__ import unicode_literals

from django.db import models
from .categories import Categories
from .itemCategory import ItemCategory
from .itemCategory import ItemCategory
from .itemPromo import ItemPromo
from .promos import Promos
from .related import Related
class Items(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=128, db_column='name')
    price = models.IntegerField(db_column='price')
    quantity = models.IntegerField(db_column='quantity')
    info = models.TextField(db_column='info', null=True)
    images = models.ImageField(db_column='images',upload_to = 'items/', null=True)
    categories = models.ManyToManyField(Categories,through=ItemCategory)
    promos = models.ManyToManyField(Promos, through=ItemPromo)
    related = models.ManyToManyField("self", through=Related, symmetrical=False, through_fields=('item','related',))
    class Meta:
        managed = False
        db_table = 'items'
    def __str__(self):
        return self.name
