from __future__ import unicode_literals

from django.db import models
class ItemCategory(models.Model):
    id = models.AutoField(primary_key=True)
    item = models.ForeignKey('Items', models.DO_NOTHING, db_column='item')
    category = models.ForeignKey('Categories', models.DO_NOTHING, db_column='category')

    class Meta:
        managed = False
        db_table = 'itemCategory'

