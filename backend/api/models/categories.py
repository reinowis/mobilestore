from __future__ import unicode_literals

from django.db import models
from .itemCategory import ItemCategory
class Categories(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=128)
    parent = models.ForeignKey('self', db_column='parent', null=True)
    cate_items = models.ManyToManyField('Items', through=ItemCategory, through_fields=('category','item'), related_name="cate_items")
    class Meta:
        managed = False
        db_table = 'categories'
    def __str__(self):
        return self.name