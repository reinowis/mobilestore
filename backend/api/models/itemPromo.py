from __future__ import unicode_literals

from django.db import models

class ItemPromo(models.Model):
    id = models.AutoField(primary_key=True)
    item = models.ForeignKey('Items', models.DO_NOTHING, db_column='item')
    promo = models.ForeignKey('Promos', models.DO_NOTHING, db_column='promo')
    class Meta:
        managed = False
        db_table = 'itemPromo'
