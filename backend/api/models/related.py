from __future__ import unicode_literals

from django.db import models

class Related(models.Model):
    id = models.AutoField(primary_key=True)
    item = models.ForeignKey('Items', models.DO_NOTHING, db_column='item', related_name='item')
    related = models.ForeignKey('Items', models.DO_NOTHING, db_column='related', related_name='item_related')

    class Meta:
        managed = False
        db_table = 'related'
