from __future__ import unicode_literals

from django.db import models
from .items import Items
class OrderDetail(models.Model):
    id = models.AutoField(primary_key=True)
    orders = models.ForeignKey('Orders', models.DO_NOTHING, db_column='orders')
    item = models.ForeignKey(Items, models.DO_NOTHING, db_column='item')
    price = models.IntegerField(default=0)
    quantity = models.IntegerField()
    total = models.IntegerField(default=0)

    class Meta:
        managed = False
        db_table = 'orderDetail'