from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from datetime import datetime
class Orders(models.Model):
    id = models.AutoField(primary_key=True)
    date = models.DateTimeField(default=datetime.now())
    customer = models.ForeignKey(User, models.DO_NOTHING, db_column='customer')
    phone = models.CharField(max_length=16)
    address = models.CharField(max_length=256)
    price = models.IntegerField(default=0)
    status = models.IntegerField(default=0)
    class Meta:
        managed = False
        db_table = 'orders'
