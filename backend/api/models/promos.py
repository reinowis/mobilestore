from __future__ import unicode_literals

from django.db import models
from .itemPromo import ItemPromo
class Promos(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=128)
    date_start = models.DateField()
    date_end = models.DateField()
    dates = models.CharField(max_length=8, default="")
    time_start = models.TimeField()
    time_end = models.TimeField()
    value = models.IntegerField(default=0)
    promo_items = models.ManyToManyField('Items', through=ItemPromo, through_fields=('promo','item'), related_name="promo_items")
    class Meta:
        managed = False
        db_table = 'promos'
    def __str__(self):
        return self.name    
