from django.shortcuts import get_object_or_404
from rest_framework.viewsets import ModelViewSet, ReadOnlyModelViewSet
from rest_framework.response import Response
from api.serializers import CategoriesSerializer, ItemCategorySerializer, RelatedSerializer, ItemPromoSerializer, PromosSerializer, ItemsSerializer, OrderDetailSerializer, OrdersSerializer, RelatedSerializer
from api.models import Categories, ItemCategory, ItemPromo, Promos, Items, OrderDetail, Orders, Related
import rest_framework_filters as filters
from rest_framework.permissions import IsAdminUser, IsAuthenticated
class ItemPromoFilter(filters.FilterSet):
    class Meta:
        model = ItemPromo
        fields = '__all__'
class ItemPromoViewSet(ModelViewSet):
    queryset = ItemPromo.objects.all()
    serializer_class = ItemPromoSerializer
    filter_class = ItemPromoFilter
    permission_classes = (IsAuthenticated, IsAdminUser,)
class UserItemPromoViewSet(ReadOnlyModelViewSet):
    queryset = ItemPromo.objects.all()
    serializer_class = ItemPromoSerializer
    filter_class = ItemPromoFilter
