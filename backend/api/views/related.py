from django.shortcuts import get_object_or_404
from rest_framework.viewsets import ModelViewSet, ReadOnlyModelViewSet
from rest_framework.response import Response
from api.serializers import CategoriesSerializer, ItemCategorySerializer, RelatedSerializer, ItemPromoSerializer, PromosSerializer, ItemsSerializer, OrderDetailSerializer, OrdersSerializer, RelatedSerializer
from api.models import Categories, ItemCategory, ItemPromo, Promos, Items, OrderDetail, Orders, Related
import rest_framework_filters as filters
from rest_framework.permissions import IsAuthenticated, IsAdminUser
class RelatedFilter(filters.FilterSet):
    class Meta:
        model = Related
        fields = '__all__'

class RelatedViewSet(ModelViewSet):
    queryset = Related.objects.all()
    serializer_class = RelatedSerializer
    filter_class = RelatedFilter
    permission_classes = (IsAuthenticated, IsAdminUser,)
class UserRelatedViewSet(ReadOnlyModelViewSet):
    queryset = Related.objects.all()
    serializer_class = RelatedSerializer
    filter_class = RelatedFilter