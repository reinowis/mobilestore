from django.shortcuts import get_object_or_404
from rest_framework.viewsets import ModelViewSet, ReadOnlyModelViewSet
from rest_framework.response import Response
from api.serializers import CategoriesSerializer, ItemCategorySerializer, RelatedSerializer, ItemPromoSerializer, PromosSerializer, ItemsSerializer, OrderDetailSerializer, OrdersSerializer, RelatedSerializer
from api.models import Categories, ItemCategory, ItemPromo, Promos, Items, OrderDetail, Orders, Related
import rest_framework_filters as filters
from rest_framework.permissions import IsAdminUser, IsAuthenticated
class ItemsFilter(filters.FilterSet):
    class Meta:
        model = Items
        fields = {'name': ['exact', 'in', 'startswith', 'contains'], 'price': '__all__', 'quantity':'__all__', 'info':'__all__', 'categories':'__all__', 'promos':'__all__', 'related':'__all__'}

class ItemsViewSet(ModelViewSet):
    queryset = Items.objects.all()
    serializer_class = ItemsSerializer
    filter_class = ItemsFilter
    permission_classes = (IsAuthenticated, IsAdminUser,)
    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)
class UserItemsViewSet(ReadOnlyModelViewSet):
    queryset = Items.objects.all()
    serializer_class = ItemsSerializer
    filter_class = ItemsFilter