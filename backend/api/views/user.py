from rest_framework.permissions import IsAuthenticated
from django.contrib.auth.models import User, Group

from django.shortcuts import get_object_or_404
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from api.serializers import CategoriesSerializer, ItemCategorySerializer, RelatedSerializer, ItemPromoSerializer, PromosSerializer, ItemsSerializer, OrderDetailSerializer, OrdersSerializer, RelatedSerializer, UserSerializer
from api.models import Categories, ItemCategory, ItemPromo, Promos, Items, OrderDetail, Orders, Related
import rest_framework_filters as filters
from rest_framework import decorators
from rest_framework.permissions import IsAdminUser, IsAuthenticated
class UserFilter(filters.FilterSet):
    class Meta:
        model = User
        fields = '__all__'

class UserViewSet(ModelViewSet):
    permission_classes = (IsAuthenticated, IsAdminUser,)
    queryset = User.objects.filter(is_staff=False)
    serializer_class = UserSerializer
    filter_class = UserFilter