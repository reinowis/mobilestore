from django.shortcuts import get_object_or_404
from rest_framework.viewsets import ModelViewSet, ReadOnlyModelViewSet
from rest_framework.response import Response
from api.serializers import CategoriesSerializer, ItemCategorySerializer, RelatedSerializer, ItemPromoSerializer, PromosSerializer, ItemsSerializer, OrderDetailSerializer, OrdersSerializer, RelatedSerializer
from api.models import Categories, ItemCategory, ItemPromo, Promos, Items, OrderDetail, Orders, Related
import rest_framework_filters as filters
from rest_framework.permissions import IsAdminUser, IsAuthenticated
class ItemCategoryFilter(filters.FilterSet):
    class Meta:
        model = ItemCategory
        fields = '__all__'
class ItemCategoryViewSet(ModelViewSet):
    queryset = ItemCategory.objects.all()
    serializer_class = ItemCategorySerializer
    filter_class = ItemCategoryFilter
    permission_classes = (IsAdminUser, IsAuthenticated,)
class UserItemCategoryViewSet(ReadOnlyModelViewSet):
    queryset = ItemCategory.objects.all()
    serializer_class = ItemCategorySerializer
    filter_class = ItemCategoryFilter    