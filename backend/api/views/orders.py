from django.shortcuts import get_object_or_404
from rest_framework.viewsets import ModelViewSet, ReadOnlyModelViewSet, GenericViewSet
from rest_framework.response import Response
from api.serializers import CategoriesSerializer, ItemCategorySerializer, RelatedSerializer, ItemPromoSerializer, PromosSerializer, ItemsSerializer, OrderDetailSerializer, OrdersSerializer, RelatedSerializer
from api.models import Categories, ItemCategory, ItemPromo, Promos, Items, OrderDetail, Orders, Related
import rest_framework_filters as filters
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from rest_framework import mixins, status

class OrdersFilter(filters.FilterSet):
    class Meta:
        model = Orders
        fields = '__all__'

class OrdersViewSet(mixins.UpdateModelMixin,mixins.RetrieveModelMixin,mixins.ListModelMixin,GenericViewSet):
    permission_classes = (IsAuthenticated, IsAdminUser,)
    queryset = Orders.objects.all()
    filter_class = OrdersFilter
    serializer_class = OrdersSerializer
class UserOrdersViewSet(mixins.CreateModelMixin,mixins.RetrieveModelMixin,mixins.ListModelMixin,GenericViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = Orders.objects.all()
    filter_class = OrdersFilter
    serializer_class = OrdersSerializer   
    def list(self, request, *args, **kwargs):
        request.data['customer'] = request.user.pk
        queryset = self.filter_queryset(self.get_queryset())
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data) 
    def create(self, request, *args, **kwargs):
        request.data['customer_id'] = request.user.pk
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)