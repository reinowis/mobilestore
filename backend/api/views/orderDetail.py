from django.shortcuts import get_object_or_404
from rest_framework.viewsets import ModelViewSet, GenericViewSet
from rest_framework.response import Response
from api.serializers import CategoriesSerializer, ItemCategorySerializer, RelatedSerializer, ItemPromoSerializer, PromosSerializer, ItemsSerializer, OrderDetailSerializer, OrdersSerializer, RelatedSerializer
from api.models import Categories, ItemCategory, ItemPromo, Promos, Items, OrderDetail, Orders, Related
import rest_framework_filters as filters
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework import mixins
class OrderDetailFilter(filters.FilterSet):
    class Meta:
        model = OrderDetail
        fields = '__all__'

class OrderDetailViewSet(mixins.UpdateModelMixin,mixins.RetrieveModelMixin,mixins.ListModelMixin,GenericViewSet):
    permission_classes = (IsAuthenticated,IsAdminUser,)
    queryset = OrderDetail.objects.all()
    filter_class = OrderDetailFilter
    serializer_class = OrderDetailSerializer
class UserOrderDetailViewSet(mixins.CreateModelMixin,mixins.RetrieveModelMixin,mixins.ListModelMixin,GenericViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = OrderDetail.objects.all()
    filter_class = OrderDetailFilter
    serializer_class = OrderDetailSerializer