from django.shortcuts import get_object_or_404
from rest_framework.viewsets import ViewSet, ModelViewSet, ReadOnlyModelViewSet
from rest_framework.response import Response
from api.serializers import CategoriesSerializer, ItemCategorySerializer, RelatedSerializer, ItemPromoSerializer, PromosSerializer, ItemsSerializer, OrderDetailSerializer, OrdersSerializer, RelatedSerializer
from api.models import Categories, ItemCategory, ItemPromo, Promos, Items, OrderDetail, Orders, Related
import rest_framework_filters as filters
from rest_framework.permissions import IsAdminUser, IsAuthenticated
class CategoriesFilter(filters.FilterSet):
    class Meta:
        model = Categories
        fields = {'name': ['exact', 'in', 'startswith', 'contains']}

class CategoriesViewSet(ModelViewSet):
    serializer_class = CategoriesSerializer
    queryset = Categories.objects.all()
    filter_class = CategoriesFilter
    permission_classes = (IsAuthenticated, IsAdminUser,)
    def put(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)
class UserCategoriesViewSet(ReadOnlyModelViewSet):
    serializer_class = CategoriesSerializer
    queryset = Categories.objects.all()
    filter_class = CategoriesFilter    