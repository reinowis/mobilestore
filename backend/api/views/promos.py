from django.shortcuts import get_object_or_404
from rest_framework.viewsets import ModelViewSet, ReadOnlyModelViewSet
from rest_framework.response import Response
from api.serializers import CategoriesSerializer, ItemCategorySerializer, RelatedSerializer, ItemPromoSerializer, PromosSerializer, ItemsSerializer, OrderDetailSerializer, OrdersSerializer, RelatedSerializer
from api.models import Categories, ItemCategory, ItemPromo, Promos, Items, OrderDetail, Orders, Related
import rest_framework_filters as filters
from rest_framework.permissions import IsAuthenticated, IsAdminUser
class PromosFilter(filters.FilterSet):
    class Meta:
        model = Promos
        fields = '__all__'

class PromosViewSet(ModelViewSet):
    permission_classes = (IsAuthenticated, IsAdminUser,)
    queryset = Promos.objects.all()
    serializer_class = PromosSerializer
    filter_class = PromosFilter
    def put(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)
class UserPromosViewSet(ReadOnlyModelViewSet):
    queryset = Promos.objects.all()
    serializer_class = PromosSerializer
    filter_class = PromosFilter