-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 15, 2017 at 06:51 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `webservice`
--

-- --------------------------------------------------------

--
-- Table structure for table `account_emailaddress`
--

CREATE TABLE `account_emailaddress` (
  `id` int(11) NOT NULL,
  `email` varchar(254) NOT NULL,
  `verified` tinyint(1) NOT NULL,
  `primary` tinyint(1) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `account_emailconfirmation`
--

CREATE TABLE `account_emailconfirmation` (
  `id` int(11) NOT NULL,
  `created` datetime(6) NOT NULL,
  `sent` datetime(6) DEFAULT NULL,
  `key` varchar(64) NOT NULL,
  `email_address_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `authtoken_token`
--

CREATE TABLE `authtoken_token` (
  `key` varchar(40) NOT NULL,
  `created` datetime(6) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `authtoken_token`
--

INSERT INTO `authtoken_token` (`key`, `created`, `user_id`) VALUES
('61feda501b3646e49c19b7f9e31d72667af5546b', '2017-12-10 03:02:09.523971', 2),
('b1e77562c3f931ed4516f25f1b153b1dc125f6c4', '2017-12-09 15:51:18.226687', 1);

-- --------------------------------------------------------

--
-- Table structure for table `auth_group`
--

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_group_permissions`
--

CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_permission`
--

CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add log entry', 1, 'add_logentry'),
(2, 'Can change log entry', 1, 'change_logentry'),
(3, 'Can delete log entry', 1, 'delete_logentry'),
(4, 'Can add user', 2, 'add_user'),
(5, 'Can change user', 2, 'change_user'),
(6, 'Can delete user', 2, 'delete_user'),
(7, 'Can add group', 3, 'add_group'),
(8, 'Can change group', 3, 'change_group'),
(9, 'Can delete group', 3, 'delete_group'),
(10, 'Can add permission', 4, 'add_permission'),
(11, 'Can change permission', 4, 'change_permission'),
(12, 'Can delete permission', 4, 'delete_permission'),
(13, 'Can add content type', 5, 'add_contenttype'),
(14, 'Can change content type', 5, 'change_contenttype'),
(15, 'Can delete content type', 5, 'delete_contenttype'),
(16, 'Can add session', 6, 'add_session'),
(17, 'Can change session', 6, 'change_session'),
(18, 'Can delete session', 6, 'delete_session'),
(19, 'Can add item subcategory', 7, 'add_itemsubcategory'),
(20, 'Can change item subcategory', 7, 'change_itemsubcategory'),
(21, 'Can delete item subcategory', 7, 'delete_itemsubcategory'),
(22, 'Can add subcategories', 8, 'add_subcategories'),
(23, 'Can change subcategories', 8, 'change_subcategories'),
(24, 'Can delete subcategories', 8, 'delete_subcategories'),
(25, 'Can add item category', 9, 'add_itemcategory'),
(26, 'Can change item category', 9, 'change_itemcategory'),
(27, 'Can delete item category', 9, 'delete_itemcategory'),
(28, 'Can add customers', 10, 'add_customers'),
(29, 'Can change customers', 10, 'change_customers'),
(30, 'Can delete customers', 10, 'delete_customers'),
(31, 'Can add orders', 11, 'add_orders'),
(32, 'Can change orders', 11, 'change_orders'),
(33, 'Can delete orders', 11, 'delete_orders'),
(34, 'Can add items', 12, 'add_items'),
(35, 'Can change items', 12, 'change_items'),
(36, 'Can delete items', 12, 'delete_items'),
(37, 'Can add promos', 13, 'add_promos'),
(38, 'Can change promos', 13, 'change_promos'),
(39, 'Can delete promos', 13, 'delete_promos'),
(40, 'Can add categories', 14, 'add_categories'),
(41, 'Can change categories', 14, 'change_categories'),
(42, 'Can delete categories', 14, 'delete_categories'),
(43, 'Can add order detail', 15, 'add_orderdetail'),
(44, 'Can change order detail', 15, 'change_orderdetail'),
(45, 'Can delete order detail', 15, 'delete_orderdetail'),
(46, 'Can add item promo', 16, 'add_itempromo'),
(47, 'Can change item promo', 16, 'change_itempromo'),
(48, 'Can delete item promo', 16, 'delete_itempromo'),
(49, 'Can add Token', 17, 'add_token'),
(50, 'Can change Token', 17, 'change_token'),
(51, 'Can delete Token', 17, 'delete_token'),
(52, 'Can add site', 18, 'add_site'),
(53, 'Can change site', 18, 'change_site'),
(54, 'Can delete site', 18, 'delete_site'),
(55, 'Can add email address', 19, 'add_emailaddress'),
(56, 'Can change email address', 19, 'change_emailaddress'),
(57, 'Can delete email address', 19, 'delete_emailaddress'),
(58, 'Can add email confirmation', 20, 'add_emailconfirmation'),
(59, 'Can change email confirmation', 20, 'change_emailconfirmation'),
(60, 'Can delete email confirmation', 20, 'delete_emailconfirmation'),
(61, 'Can add related', 21, 'add_related'),
(62, 'Can change related', 21, 'change_related'),
(63, 'Can delete related', 21, 'delete_related');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user`
--

CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_user`
--

INSERT INTO `auth_user` (`id`, `password`, `last_login`, `is_superuser`, `username`, `first_name`, `last_name`, `email`, `is_staff`, `is_active`, `date_joined`) VALUES
(1, 'pbkdf2_sha256$36000$cL4uatxEEMET$apWdbCqIveAqyO98ntrQDNQOeynC+firFzY5cVVIQsY=', '2017-12-14 15:28:38.712212', 1, 'reinowis', '', '', 'maihuynh16995@gmail.com', 1, 1, '2017-12-09 15:49:20.493777'),
(2, 'pbkdf2_sha256$36000$dqk6dzMj9Y4N$06OpGT9vStfmtZygzBlEtH8AGrBuipBt/I/2OZxVl3Y=', '2017-12-12 17:19:32.057260', 0, 'user01', 'Mai', 'Huynh', 'mousewis@gmail.com', 0, 1, '2017-12-10 03:02:09.360563');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_groups`
--

CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_user_permissions`
--

CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `parent` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `parent`) VALUES
(1, 'Cate 1', NULL),
(3, 'Cate 2', 1),
(4, 'Cate 4', 1),
(5, 'Cate 5', 1),
(6, 'Cate custom', NULL),
(7, 'Cate custom', NULL),
(8, 'Cate editable', NULL),
(9, 'Cate 10', 0),
(10, 'Cate Test', NULL),
(11, 'cate 2', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `django_admin_log`
--

CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) UNSIGNED NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `django_content_type`
--

CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(19, 'account', 'emailaddress'),
(20, 'account', 'emailconfirmation'),
(1, 'admin', 'logentry'),
(14, 'api', 'categories'),
(10, 'api', 'customers'),
(9, 'api', 'itemcategory'),
(16, 'api', 'itempromo'),
(12, 'api', 'items'),
(7, 'api', 'itemsubcategory'),
(15, 'api', 'orderdetail'),
(11, 'api', 'orders'),
(13, 'api', 'promos'),
(21, 'api', 'related'),
(8, 'api', 'subcategories'),
(3, 'auth', 'group'),
(4, 'auth', 'permission'),
(2, 'auth', 'user'),
(17, 'authtoken', 'token'),
(5, 'contenttypes', 'contenttype'),
(6, 'sessions', 'session'),
(18, 'sites', 'site');

-- --------------------------------------------------------

--
-- Table structure for table `django_migrations`
--

CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'contenttypes', '0001_initial', '2017-12-09 08:37:08.061171'),
(2, 'auth', '0001_initial', '2017-12-09 08:37:17.736813'),
(3, 'account', '0001_initial', '2017-12-09 08:37:19.982358'),
(4, 'account', '0002_email_max_length', '2017-12-09 08:37:21.263665'),
(5, 'admin', '0001_initial', '2017-12-09 08:37:23.639382'),
(6, 'admin', '0002_logentry_remove_auto_add', '2017-12-09 08:37:23.694315'),
(7, 'contenttypes', '0002_remove_content_type_name', '2017-12-09 08:37:24.757598'),
(8, 'auth', '0002_alter_permission_name_max_length', '2017-12-09 08:37:25.483440'),
(9, 'auth', '0003_alter_user_email_max_length', '2017-12-09 08:37:26.455924'),
(10, 'auth', '0004_alter_user_username_opts', '2017-12-09 08:37:26.528837'),
(11, 'auth', '0005_alter_user_last_login_null', '2017-12-09 08:37:27.086689'),
(12, 'auth', '0006_require_contenttypes_0002', '2017-12-09 08:37:27.183198'),
(13, 'auth', '0007_alter_validators_add_error_messages', '2017-12-09 08:37:27.328152'),
(14, 'auth', '0008_alter_user_username_max_length', '2017-12-09 08:37:28.007849'),
(15, 'api', '0001_initial', '2017-12-09 08:37:28.095090'),
(16, 'authtoken', '0001_initial', '2017-12-09 08:37:29.336969'),
(17, 'authtoken', '0002_auto_20160226_1747', '2017-12-09 08:37:30.387123'),
(18, 'sessions', '0001_initial', '2017-12-09 08:37:30.890622'),
(19, 'sites', '0001_initial', '2017-12-09 08:37:31.386601'),
(20, 'sites', '0002_alter_domain_unique', '2017-12-09 08:37:31.620110'),
(21, 'api', '0002_auto_20171209_0838', '2017-12-09 08:38:13.739457'),
(22, 'api', '0002_auto_20171209_1638', '2017-12-09 16:38:23.538140');

-- --------------------------------------------------------

--
-- Table structure for table `django_session`
--

CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `django_session`
--

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`) VALUES
('11rfvv04epsvdtfshvpczo2tjprb384p', 'ZWFjYTcyMmZmZTIyM2FhYzU1MDZmNzRhMTgzYjMzY2M5NTIyMzM2Yzp7Il9hdXRoX3VzZXJfaGFzaCI6ImJmZTZkZmRhZGMzYzA5ZTVkOTkxM2Q4MTZkMTEwNmJkNjVlYzUxNGMiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=', '2017-12-26 10:48:37.692048'),
('644bbt1k11eftm3db840nr5j06xehddw', 'MDcyNTAxODA0ODRmMDJmMGI5NGJlODc5ZmQwNDJjYTdkYWQyY2NiNjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJiZmU2ZGZkYWRjM2MwOWU1ZDk5MTNkODE2ZDExMDZiZDY1ZWM1MTRjIn0=', '2017-12-28 15:28:38.849196'),
('bvp9mznkyma0v4nyqdww9v1rcsz4z7fo', 'MDcyNTAxODA0ODRmMDJmMGI5NGJlODc5ZmQwNDJjYTdkYWQyY2NiNjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJiZmU2ZGZkYWRjM2MwOWU1ZDk5MTNkODE2ZDExMDZiZDY1ZWM1MTRjIn0=', '2017-12-26 18:27:04.121886'),
('c8gptrdxc22mq7i5bgtnsgz8zmiym2wk', 'MDcyNTAxODA0ODRmMDJmMGI5NGJlODc5ZmQwNDJjYTdkYWQyY2NiNjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJiZmU2ZGZkYWRjM2MwOWU1ZDk5MTNkODE2ZDExMDZiZDY1ZWM1MTRjIn0=', '2017-12-25 16:35:48.586537'),
('ddq8f4nk5v7d4vp4hgiaovvz6htf3v4j', 'NmY3MTAzZjFlMDAwYzkxMzc1ZDFjNTAxMjY1MjE5MGEyYmVhY2JjZDp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiYmZlNmRmZGFkYzNjMDllNWQ5OTEzZDgxNmQxMTA2YmQ2NWVjNTE0YyIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=', '2017-12-23 15:51:18.550454'),
('f28bywj75o1dbsnqs79qjhm46sahvqf7', 'ZWFjYTcyMmZmZTIyM2FhYzU1MDZmNzRhMTgzYjMzY2M5NTIyMzM2Yzp7Il9hdXRoX3VzZXJfaGFzaCI6ImJmZTZkZmRhZGMzYzA5ZTVkOTkxM2Q4MTZkMTEwNmJkNjVlYzUxNGMiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=', '2017-12-26 10:56:07.680176'),
('hbm6bjuutpvqmnobwymb7j54yfbz92px', 'MDcyNTAxODA0ODRmMDJmMGI5NGJlODc5ZmQwNDJjYTdkYWQyY2NiNjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJiZmU2ZGZkYWRjM2MwOWU1ZDk5MTNkODE2ZDExMDZiZDY1ZWM1MTRjIn0=', '2017-12-25 15:53:23.121908'),
('hty7woss4hxtdh6b6aenkvagxmzbef81', 'NDRlN2JjMmRjZmZjN2QyYjZhZTNmMmQ5OGIxMTkzNzY0MWVkNjc5Mjp7Il9hdXRoX3VzZXJfaGFzaCI6ImY0OTQwZWNmMzBmYTkyYTg2ZjcyZTgyZDVlOGUzNzY0NTBkM2ExMDEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIyIn0=', '2017-12-26 17:19:32.280221'),
('ij99beobp2aypnqyhmllpgqqa28ve9ho', 'ZWFjYTcyMmZmZTIyM2FhYzU1MDZmNzRhMTgzYjMzY2M5NTIyMzM2Yzp7Il9hdXRoX3VzZXJfaGFzaCI6ImJmZTZkZmRhZGMzYzA5ZTVkOTkxM2Q4MTZkMTEwNmJkNjVlYzUxNGMiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=', '2017-12-23 17:26:17.151604'),
('jl8zfg127pn4iffek8haq74w9637eqjl', 'NzUyMWIzMjljYzE5ODE3MWRjNWZkNWUyM2Q5NjdlOGJkZjlhMjllZTp7Il9hdXRoX3VzZXJfaGFzaCI6ImJmZTZkZmRhZGMzYzA5ZTVkOTkxM2Q4MTZkMTEwNmJkNjVlYzUxNGMiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=', '2017-12-24 03:07:06.666874'),
('l6o836uubmh2nj3pd7f4bwvv5ckrbw5i', 'NDM2MmU1MmEyNWE3MjcwZGI5ODBhZTVlOWYzYmRjN2JlZWYxNDZkNjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiJiZmU2ZGZkYWRjM2MwOWU1ZDk5MTNkODE2ZDExMDZiZDY1ZWM1MTRjIn0=', '2017-12-24 15:10:53.686767'),
('l8eazfyw9h2pcn7u8e4e2z7r0zutwwzr', 'MTZhYTU5NjJmMzIzZGEzNmE4Y2NiODZiMzE1YjY1Y2JiMGQxODhhZDp7Il9hdXRoX3VzZXJfaGFzaCI6ImY0OTQwZWNmMzBmYTkyYTg2ZjcyZTgyZDVlOGUzNzY0NTBkM2ExMDEiLCJhY2NvdW50X3ZlcmlmaWVkX2VtYWlsIjpudWxsLCJfYXV0aF91c2VyX2lkIjoiMiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiYWNjb3VudF91c2VyIjoiMiJ9', '2017-12-24 03:02:09.725726'),
('ouzdm2zfxnjemrc268clkddl8whn580o', 'MDcyNTAxODA0ODRmMDJmMGI5NGJlODc5ZmQwNDJjYTdkYWQyY2NiNjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJiZmU2ZGZkYWRjM2MwOWU1ZDk5MTNkODE2ZDExMDZiZDY1ZWM1MTRjIn0=', '2017-12-27 11:57:33.830501'),
('v3i4zo182u89ztd1kbwdocixpk3iaq48', 'MDcyNTAxODA0ODRmMDJmMGI5NGJlODc5ZmQwNDJjYTdkYWQyY2NiNjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJiZmU2ZGZkYWRjM2MwOWU1ZDk5MTNkODE2ZDExMDZiZDY1ZWM1MTRjIn0=', '2017-12-26 18:25:30.254525');

-- --------------------------------------------------------

--
-- Table structure for table `django_site`
--

CREATE TABLE `django_site` (
  `id` int(11) NOT NULL,
  `domain` varchar(100) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `django_site`
--

INSERT INTO `django_site` (`id`, `domain`, `name`) VALUES
(1, 'example.com', 'example.com');

-- --------------------------------------------------------

--
-- Table structure for table `itemCategory`
--

CREATE TABLE `itemCategory` (
  `id` int(11) NOT NULL,
  `item` int(11) NOT NULL,
  `category` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `itemCategory`
--

INSERT INTO `itemCategory` (`id`, `item`, `category`) VALUES
(9, 10, 1),
(18, 2, 1),
(20, 11, 1),
(21, 1, 3),
(22, 1, 5),
(23, 13, 5);

-- --------------------------------------------------------

--
-- Table structure for table `itemPromo`
--

CREATE TABLE `itemPromo` (
  `id` int(11) NOT NULL,
  `item` int(11) NOT NULL,
  `promo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `itemPromo`
--

INSERT INTO `itemPromo` (`id`, `item`, `promo`) VALUES
(9, 1, 2),
(13, 11, 1),
(14, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `price` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `info` varchar(1024) DEFAULT NULL,
  `images` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `name`, `price`, `quantity`, `info`, `images`) VALUES
(1, 'Item 1 edit', 10000, 32, 'Item : 0/ width: 2cm', ''),
(2, 'Item 2', 10000, 5, 'Item : 0/ width: 2cm', 'item_1.jpg'),
(3, 'Item 3', 15000, 10, 'No info here', 'items/photo.jpg'),
(10, 'Item 5', 50000, 10, 'no info', 'items/retro_november-540x337_DgubrKs.png'),
(11, 'Item 5', 50000, 10, 'no info', 'items/retro_november-540x337_u6Gf8Xr.png'),
(12, 'Item 5', 50000, 10, 'no info', 'items/retro_november-540x337_zsbkI97.png'),
(13, 'Item 5', 50000, 10, 'no info', 'items/photo_itCWstH.jpg'),
(14, 'Item 5', 50000, 10, 'no info', 'items/photo_upPg6CW.jpg'),
(15, 'Item 7', 70000, 7, 'no', 'items/retro_november-540x337_xHxl3CG.png'),
(16, 'Item 7', 70000, 7, 'no', 'items/retro_november-540x337_2HNhmzV.png'),
(17, 'Item 5', 50000, 10, 'no', 'items/retro_november-540x337_WFWZIxX.png'),
(18, 'Item 10', 5555555, 6, 'no', 'items/retro_november-540x337_OJpMzxa.png'),
(19, 'Item 10', 78000, 5, 'no', 'items/Screenshot_from_2017-11-06_19-26-35.png'),
(20, 'Item 3', 58200, 74, 'no', 'items/retro_november-540x337_62ufC9y.png'),
(21, 'Item 7', 589000, 58, 'no', 'items/Screenshot_from_2017-10-14_16-11-17.png'),
(22, 'item 14', 10000, 5, NULL, ''),
(23, 'item 14', 10000, 5, NULL, ''),
(24, 'Item 10 edit', 10000, 50, NULL, ''),
(25, 'Item 10 edit', 10000, 50, NULL, ''),
(26, 'Item 10 edit', 10000, 50, NULL, ''),
(27, 'Item 10 edit', 10000, 50, NULL, ''),
(28, 'Item 10 edit', 10000, 50, NULL, ''),
(29, 'Item 10 edit', 10000, 50, NULL, ''),
(30, 'Item 10 edit', 10000, 50, NULL, ''),
(31, 'Item 10 edit', 10000, 50, NULL, ''),
(32, 'Item 10 edit', 10000, 50, NULL, ''),
(33, 'Item 10 edit', 10000, 50, NULL, ''),
(34, 'Item 10 edit', 10000, 50, NULL, ''),
(35, 'Item 10 edit', 10000, 50, NULL, ''),
(36, 'Item 1210', 18000, 5, NULL, ''),
(37, 'Item 1210', 18000, 5, NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `orderDetail`
--

CREATE TABLE `orderDetail` (
  `id` int(11) NOT NULL,
  `orders` int(11) NOT NULL,
  `item` int(11) NOT NULL,
  `price` int(11) NOT NULL DEFAULT '0',
  `quantity` int(11) NOT NULL DEFAULT '0',
  `total` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orderDetail`
--

INSERT INTO `orderDetail` (`id`, `orders`, `item`, `price`, `quantity`, `total`) VALUES
(6, 1, 1, 8000, 5, 40000),
(11, 8, 1, 8000, 3, 24000);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `customer` int(11) NOT NULL,
  `phone` varchar(16) NOT NULL,
  `address` varchar(256) NOT NULL,
  `price` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `date`, `customer`, `phone`, `address`, `price`, `status`) VALUES
(1, '2017-12-21 00:00:00', 2, '0946607797', 'Ben Tre', 40000, -1),
(3, '2017-12-10 14:41:16', 2, '0946879526', 'Ben Tre', 0, 1),
(8, '2017-12-10 14:57:03', 2, '0946879526', 'Ben Tre', 24000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `promos`
--

CREATE TABLE `promos` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `dates` varchar(8) NOT NULL,
  `time_start` time NOT NULL,
  `time_end` time NOT NULL,
  `value` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `promos`
--

INSERT INTO `promos` (`id`, `name`, `date_start`, `date_end`, `dates`, `time_start`, `time_end`, `value`) VALUES
(1, 'Promo 1', '2017-12-15', '2018-01-06', '025634', '01:00:00', '23:00:00', 10),
(2, 'Promo 2', '2017-12-01', '2017-12-31', '0123456', '00:00:00', '23:00:00', 10),
(3, 'Promo 3', '2017-12-13', '2017-12-27', '01', '01:36:01', '13:36:06', 0),
(4, 'Promo 2', '2017-12-07', '2017-12-28', '', '00:23:35', '11:23:39', 10);

-- --------------------------------------------------------

--
-- Table structure for table `related`
--

CREATE TABLE `related` (
  `id` int(11) NOT NULL,
  `item` int(11) NOT NULL,
  `related` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `related`
--

INSERT INTO `related` (`id`, `item`, `related`) VALUES
(1, 30, 1),
(2, 31, 1),
(3, 33, 1),
(4, 35, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account_emailaddress`
--
ALTER TABLE `account_emailaddress`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `account_emailaddress_user_id_2c513194_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `account_emailconfirmation`
--
ALTER TABLE `account_emailconfirmation`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `key` (`key`),
  ADD KEY `account_emailconfirm_email_address_id_5b7f8c58_fk_account_e` (`email_address_id`);

--
-- Indexes for table `authtoken_token`
--
ALTER TABLE `authtoken_token`
  ADD PRIMARY KEY (`key`),
  ADD UNIQUE KEY `user_id` (`user_id`);

--
-- Indexes for table `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  ADD KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`);

--
-- Indexes for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`);

--
-- Indexes for table `auth_user`
--
ALTER TABLE `auth_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  ADD KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`);

--
-- Indexes for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  ADD KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  ADD KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `django_content_type`
--
ALTER TABLE `django_content_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`);

--
-- Indexes for table `django_migrations`
--
ALTER TABLE `django_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `django_session`
--
ALTER TABLE `django_session`
  ADD PRIMARY KEY (`session_key`),
  ADD KEY `django_session_expire_date_a5c62663` (`expire_date`);

--
-- Indexes for table `django_site`
--
ALTER TABLE `django_site`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_site_domain_a2e37b91_uniq` (`domain`);

--
-- Indexes for table `itemCategory`
--
ALTER TABLE `itemCategory`
  ADD PRIMARY KEY (`id`),
  ADD KEY `item` (`item`),
  ADD KEY `category` (`category`);

--
-- Indexes for table `itemPromo`
--
ALTER TABLE `itemPromo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `item` (`item`),
  ADD KEY `promo` (`promo`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orderDetail`
--
ALTER TABLE `orderDetail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders` (`orders`),
  ADD KEY `item` (`item`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer` (`customer`);

--
-- Indexes for table `promos`
--
ALTER TABLE `promos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `related`
--
ALTER TABLE `related`
  ADD PRIMARY KEY (`id`),
  ADD KEY `item` (`item`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account_emailaddress`
--
ALTER TABLE `account_emailaddress`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `account_emailconfirmation`
--
ALTER TABLE `account_emailconfirmation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_permission`
--
ALTER TABLE `auth_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;
--
-- AUTO_INCREMENT for table `auth_user`
--
ALTER TABLE `auth_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `django_content_type`
--
ALTER TABLE `django_content_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `django_migrations`
--
ALTER TABLE `django_migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `django_site`
--
ALTER TABLE `django_site`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `itemCategory`
--
ALTER TABLE `itemCategory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `itemPromo`
--
ALTER TABLE `itemPromo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `orderDetail`
--
ALTER TABLE `orderDetail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `promos`
--
ALTER TABLE `promos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `related`
--
ALTER TABLE `related`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `account_emailaddress`
--
ALTER TABLE `account_emailaddress`
  ADD CONSTRAINT `account_emailaddress_user_id_2c513194_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `account_emailconfirmation`
--
ALTER TABLE `account_emailconfirmation`
  ADD CONSTRAINT `account_emailconfirm_email_address_id_5b7f8c58_fk_account_e` FOREIGN KEY (`email_address_id`) REFERENCES `account_emailaddress` (`id`);

--
-- Constraints for table `authtoken_token`
--
ALTER TABLE `authtoken_token`
  ADD CONSTRAINT `authtoken_token_user_id_35299eff_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`);

--
-- Constraints for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Constraints for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  ADD CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `itemCategory`
--
ALTER TABLE `itemCategory`
  ADD CONSTRAINT `itemCategory_ibfk_1` FOREIGN KEY (`item`) REFERENCES `items` (`id`),
  ADD CONSTRAINT `itemCategory_ibfk_2` FOREIGN KEY (`category`) REFERENCES `categories` (`id`);

--
-- Constraints for table `itemPromo`
--
ALTER TABLE `itemPromo`
  ADD CONSTRAINT `itemPromo_ibfk_1` FOREIGN KEY (`item`) REFERENCES `items` (`id`),
  ADD CONSTRAINT `itemPromo_ibfk_2` FOREIGN KEY (`promo`) REFERENCES `promos` (`id`);

--
-- Constraints for table `orderDetail`
--
ALTER TABLE `orderDetail`
  ADD CONSTRAINT `orderDetail_ibfk_1` FOREIGN KEY (`orders`) REFERENCES `orders` (`id`),
  ADD CONSTRAINT `orderDetail_ibfk_2` FOREIGN KEY (`item`) REFERENCES `items` (`id`);

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`customer`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `related`
--
ALTER TABLE `related`
  ADD CONSTRAINT `related_ibfk_1` FOREIGN KEY (`item`) REFERENCES `items` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
